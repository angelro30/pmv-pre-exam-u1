package com.example.preexamenu1;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboActivity extends AppCompatActivity {
    private TextView lblNombre, lblNumRecibo, lblSubtotal, lblImpuesto, lblTotalAPagar;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private EditText txtHorasTrabajadas, txtHorasExtras;

    private RadioButton rdbAuxiliar, rdbAlbanil, rdbIngObra;

    private ReciboNomina reciboNomina;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(v -> calcular());
        btnLimpiar.setOnClickListener(v -> limpiar());
        btnRegresar.setOnClickListener(v -> finish());

        // Inicializar el objeto ReciboNomina antes de usarlo
        reciboNomina = new ReciboNomina();

        // Obtener el nombre desde el intent y establecerlo en el recibo
        String nombre = getIntent().getStringExtra("nombre");
        reciboNomina.setNombre(nombre);
        lblNombre.setText("Nombre: " + nombre);

        // Generar y establecer el número de recibo
        int numRecibo = reciboNomina.generarNumRecibo();
        lblNumRecibo.setText("Numero de recibo: " + numRecibo);
    }

    private void initComponents() {
        lblNombre = findViewById(R.id.lblNombre);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotalAPagar = findViewById(R.id.lblTotalAPagar);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);

        rdbAuxiliar = findViewById(R.id.auxiliar);
        rdbAlbanil = findViewById(R.id.albanil);
        rdbIngObra = findViewById(R.id.ing_obra);

        rdbAuxiliar.setSelected(true);
    }

    private void calcular() {
        String horasTrabajadasStr = txtHorasTrabajadas.getText().toString().trim();
        String horasExtrasStr = txtHorasExtras.getText().toString().trim();

        if (TextUtils.isEmpty(horasTrabajadasStr) || TextUtils.isEmpty(horasExtrasStr)) {
            Toast.makeText(this, "Llene todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }
        if (!rdbAuxiliar.isChecked() && !rdbAlbanil.isChecked() && !rdbIngObra.isChecked()){
            Toast.makeText(this, "Ingrese el puesto", Toast.LENGTH_SHORT).show();
            return;
        }
        float horasTrabajadas = Float.parseFloat(horasTrabajadasStr);
        float horasExtras = Float.parseFloat(horasExtrasStr);

        int puesto = 0;
        if (rdbAuxiliar.isChecked()) {
            puesto = 1;
        } else if (rdbAlbanil.isChecked()) {
            puesto = 2;
        } else if (rdbIngObra.isChecked()) {
            puesto = 3;
        }

        reciboNomina.setHorasTrabNormal(horasTrabajadas);
        reciboNomina.setHorasTrabExtras(horasExtras);
        reciboNomina.setPuesto(puesto);

        float subtotal = reciboNomina.calcularSubtotal();
        float impuesto = reciboNomina.calcularImpuesto();
        float total = reciboNomina.calcularTotal();

        lblSubtotal.setText(String.format("Subtotal: %.2f", subtotal));
        lblImpuesto.setText(String.format("Impuesto: %.2f", impuesto));
        lblTotalAPagar.setText(String.format("Total a Pagar: %.2f", total));
    }

    private void limpiar() {
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        rdbAuxiliar.setSelected(true);
        lblSubtotal.setText("Subtotal:");
        lblImpuesto.setText("Impuesto:");
        lblTotalAPagar.setText("Total a Pagar:");
    }
}
