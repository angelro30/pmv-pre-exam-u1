package com.example.preexamenu1;

import java.util.Random;

public class ReciboNomina {
    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtras, impuestoPorc;

    private static final float PAGO_BASE = 200;
    private static final float IMPUESTO_PORCENTAJE = 0.16f;

    // Getter para numRecibo
    public int generarNumRecibo() {
        Random r = new Random();
        this.numRecibo = r.nextInt(1000);
        return this.numRecibo;
    }

    // Setter para puesto
    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    // Getter para puesto
    public int getPuesto() {
        return puesto;
    }

    // Setter para nombre
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // Getter para nombre
    public String getNombre() {
        return nombre;
    }

    // Setter para horasTrabNormal
    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    // Getter para horasTrabNormal
    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    // Setter para horasTrabExtras
    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    // Getter para horasTrabExtras
    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    // Setter para impuestoPorc
    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    // Getter para impuestoPorc
    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public float calcularSubtotal() {
        float pagoPorHora = PAGO_BASE;
        switch (puesto) {
            case 1:
                pagoPorHora += PAGO_BASE * 0.2f;
                System.out.println(pagoPorHora);
                break;
            case 2:
                pagoPorHora += PAGO_BASE * 0.5f;
                break;
            case 3:
                pagoPorHora += PAGO_BASE;
                break;
        }
        return (horasTrabNormal * pagoPorHora) + (horasTrabExtras * pagoPorHora * 2);
    }

    public float calcularImpuesto() {
        return calcularSubtotal() * IMPUESTO_PORCENTAJE;
    }

    public float calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }
}
